<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Simple PHP Calculator</title>
</head>
<body>
    <form action="index.php" method="POST">
        First number:
        <br>
        <input type="text" name="number1">
        <br>
        Second number:
        <br>
        <input type="text" name="number2">
        <br>
        Operation type:
        <br>
        <input type="radio" name="operation_type" value="+"> +
        <br>
        <input type="radio" name="operation_type" value="-"> -
        <br>
        <input type="radio" name="operation_type" value="*"> ×
        <br>
        <input type="radio" name="operation_type" value="/"> ÷
        <br>
        <input type="submit" name="submit" value="=">
    </form>
    <?php
    $firstNumber = $_REQUEST['number1'];
    $secondNumber = $_REQUEST['number2'];
    $operationType = $_REQUEST['operation_type'];
    $result = null;
     switch ($operationType) {
        case '+':
            $result = $firstNumber + $secondNumber;
            break;
        case '-':
            $result = $firstNumber - $secondNumber;
            break;
        case '*':
            $result = $firstNumber * $secondNumber;
            break;
        case '/':
            $result = $firstNumber / $secondNumber;
            break;
        default:
            $result = "Undefined operation!";
      }
      echo 'Result is <strong style="color:#ffde57">' . $result . '</strong>';
    ?>
</body>
</html>
